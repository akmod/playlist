package com.Kuba;

import java.util.ArrayList;

public class Album {
    private String albumName;
    private ArrayList <Song> newAlbum;
//    private String[] newAlbum;

    public Album(String albumName) {
        this.albumName = albumName;
//        this.newAlbum =  new String[albumSize];
        this.newAlbum = new ArrayList<>();
//        this.newAlbum = albumName;
//        this.newAlbum = new ArrayList<Song>();

    }

    public void addSong(String songName, double songDuration){
        Song newSong = new Song(songName,songDuration);
        newAlbum.add(newSong);
    }


    public String getAlbumName() {
        return albumName;
    }


    public ArrayList<Song> getNewAlbum() {
        return newAlbum;
    }
}
