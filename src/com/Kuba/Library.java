package com.Kuba;

import java.util.ArrayList;

public class Library {
    private ArrayList <Album> myLibrary;

    public Library() {
        this.myLibrary = new ArrayList<>();
        new ArrayList<Song>();
    }

    public void addAlbum (String albumName){
    Album newAlbum = new Album(albumName);
        myLibrary.add(newAlbum);
    }

    public int returnAlbum (String albumName){
        for (int i=0; i < myLibrary.size();i++){
           if (myLibrary.get(i).getAlbumName().equals(albumName)){
               return i;
           }
        }
        return -1;
    }



    public ArrayList<Album> getMyLibrary() {
        return myLibrary;
    }
}
